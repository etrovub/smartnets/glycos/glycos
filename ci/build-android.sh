#!/bin/bash
set -e

rustc --version
cargo --version

# Install prereqs
apt update
apt-get install -y build-essential libboost-dev unzip

# Install cargo config
mkdir -p .cargo

# Install Android NDK
pkgname=android-ndk
pkgver=r18.b
curl -O "https://dl.google.com/android/repository/${pkgname}-${pkgver/./}-linux-x86_64.zip"
unzip -q ${pkgname}-${pkgver/./}-linux-x86_64.zip

# Setup Android NDK
export ANDROID_NDK="$PWD/$pkgname-${pkgver/./}"
export PATH=$PATH:$ANDROID_NDK
# Some programs such as gradle ask this as well:
export ANDROID_NDK_HOME=$ANDROID_NDK

install_toolchain() {
    # Install standalone toolchain
    python $ANDROID_NDK/build/tools/make_standalone_toolchain.py \
        --api=21 \
        --install-dir=android-21-toolchain-$2 \
        --arch=$2 \
        --force

    # Install Rust target
    rustup target add --toolchain=nightly $1-linux-androideabi
    PATH=${PATH}:${PWD}/android-21-toolchain-$2/bin/

    # for use in cc-rs
    CC_NAME=CC_$1_linux_androideabi
    AR_NAME=AR_$1_linux_androideabi

    # For cargo: like "CARGO_TARGET_I686_LINUX_ANDROID_CC".  This is really weakly
    # documented; see https://github.com/rust-lang/cargo/issues/5690 and follow
    # links from there.
    CARGO_CC_NAME=CARGO_TARGET_${1^^}_LINUX_ANDROIDEABI_CC
    CARGO_AR_NAME=CARGO_TARGET_${1^^}_LINUX_ANDROIDEABI_AR
    CARGO_LINKER_NAME=CARGO_TARGET_${1^^}_LINUX_ANDROIDEABI_LINKER

    printf -v $CC_NAME $2-linux-androideabi-clang
    printf -v $CARGO_CC_NAME $2-linux-androideabi-clang
    printf -v $AR_NAME $2-linux-androideabi-ar
    printf -v $CARGO_AR_NAME $2-linux-androideabi-ar
    printf -v $CARGO_LINKER_NAME $2-linux-androideabi-clang

    echo $CC_NAME = ${!CC_NAME}
    echo $AR_NAME = ${!AR_NAME}
    echo $CARGO_CC_NAME = ${!CARGO_CC_NAME}
    echo $CARGO_AR_NAME = ${!CARGO_AR_NAME}
    echo $CARGO_LINKER_NAME = ${!CARGO_LINKER_NAME}

    export $CC_NAME
    export $AR_NAME
    export $CARGO_CC_NAME
    export $CARGO_AR_NAME
    export $CARGO_LINKER_NAME
}

install_toolchain armv7 arm

cargo +nightly build \
    --target=armv7-linux-androideabi \
    --no-default-features \
    --features=daemon \
    --release --bin glycosd
