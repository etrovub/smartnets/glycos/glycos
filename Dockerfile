FROM rust as builder

RUN cargo --version
RUN rustc --version

COPY . source

RUN cd source && cargo build --release --all --all-features --bins

## Install

FROM debian:stretch-slim

COPY --from=builder /source/target/release/glycosd /usr/local/bin/glycosd
COPY --from=builder /source/target/release/glycos-explore /usr/local/bin/glycos-explore
