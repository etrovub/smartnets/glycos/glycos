# Glycos [![coverage report](https://gitlab.com/glycos/glycos/badges/master/coverage.svg)](https://gitlab.com/glycos/glycos/commits/master) [![pipeline status](https://gitlab.com/glycos/glycos/badges/master/pipeline.svg)](https://gitlab.com/glycos/glycos/commits/master) [![development documentation](https://img.shields.io/badge/doc-development-blue)](https://dev.glycos.org) [![public documentation](https://img.shields.io/badge/doc-public-blue)](https://doc.glycos.org)

Glycos aims to provide a platform to build privacy-friendly peer-to-peer online social networks.
In a sense, it is the equivalent of the RDBMS of centralised platforms.

## Contact and Community

If you're interested in having a chat, feel free to join our [Matrix room #glycos:rubdos.be](https://matrix.to/#/#glycos:rubdos.be).

## Publications

Glycos is a research project from the [ETRO](http://www.etrovub.be/RESEARCH/IRIS/SmartNets/) department of
the [Vrije Universiteit Brussel](https://www.vub.be/).

[Glycos: The Basis for a Peer-to-Peer, Private Online Social Network](https://link.springer.com/chapter/10.1007/978-3-030-16744-8_9)
