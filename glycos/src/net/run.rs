use std::time::Duration;

use libp2p::identity::Keypair;
// use libp2p::kad::record::Key;
use libp2p::kad::KademliaConfig;
use libp2p::mdns::{Mdns, MdnsConfig};
use libp2p::swarm::SwarmBuilder;
use libp2p::{Multiaddr, PeerId, Transport};

use crate::graph::persist::PersistentGraph;

pub use super::control::NodeFacade;
use super::GlycosSwarm;

/// Builder pattern for a Glycos-infused [`Swarm`] and [`ServerControl`].
///
///
/// ## Example
/// ```rust
/// # tokio_test::block_on(async {
/// use glycos::net::NodeBuilder;
/// let swarm = NodeBuilder::default()
///     .listen_port(10_000) // Listen on [::]:10000
///     // .graph(storage) // If you want a custom storage location
///     .into_facade()
///     .await
///     .expect("Start node");
/// # });
/// ```
pub struct NodeBuilder {
    pub db: PersistentGraph,
    pub listen_on: Vec<Multiaddr>,
    pub key_pair: Keypair,
}

impl Default for NodeBuilder {
    fn default() -> Self {
        NodeBuilder {
            db: PersistentGraph::temp(),
            listen_on: Vec::new(),
            key_pair: Keypair::generate_ed25519(),
        }
        .listen_port(0)
    }
}

impl NodeBuilder {
    /// Replaces all listeners with listeners on a specific port.
    pub fn listen_port(mut self, p: u16) -> Self {
        self.listen_on.clear();

        let v4: Multiaddr = format!("/ip4/0.0.0.0/tcp/{}", p).parse().unwrap();
        let v6: Multiaddr = format!("/ip6/::/tcp/{}", p).parse().unwrap();
        self.listen_on.push(v4);
        self.listen_on.push(v6);
        self
    }

    pub fn graph(mut self, db: PersistentGraph) -> Self {
        self.db = db;
        self
    }

    pub fn key_pair(mut self, pair: Keypair) -> Self {
        self.key_pair = pair;
        self
    }

    pub async fn into_facade(self) -> Result<NodeFacade, anyhow::Error> {
        Ok(NodeFacade::spawn(self.build().await?).await?)
    }

    pub async fn build(self) -> Result<GlycosSwarm, anyhow::Error> {
        let local_peer_id = PeerId::from(self.key_pair.public());
        log::info!("Starting peer {:?}", local_peer_id);

        let transport = {
            let tcp = libp2p::tcp::TokioTcpConfig::new()
                .nodelay(true)
                // Port reuse can be re-enabled in libp2p 0.42
                // https://github.com/libp2p/rust-libp2p/pull/2382
                .port_reuse(false);
            let dns_tcp = libp2p::dns::TokioDnsConfig::system(tcp)?;
            let ws_dns_tcp = libp2p::websocket::WsConfig::new(dns_tcp.clone());
            dns_tcp.or_transport(ws_dns_tcp)
        };

        let relay_config = libp2p::relay::RelayConfig::default();

        let (transport, relay) =
            libp2p::relay::new_transport_and_behaviour(relay_config, transport);

        let noise_keys = libp2p::noise::Keypair::<libp2p::noise::X25519Spec>::new()
            .into_authentic(&self.key_pair)
            .expect("Signing libp2p-noise static DH keypair failed.");

        let transport = transport
            .upgrade(libp2p::core::upgrade::Version::V1)
            .authenticate(libp2p::noise::NoiseConfig::xx(noise_keys).into_authenticated())
            .multiplex(libp2p::core::upgrade::SelectUpgrade::new(
                libp2p::yamux::YamuxConfig::default(),
                libp2p::mplex::MplexConfig::default(),
            ))
            .timeout(Duration::from_secs(20))
            .boxed();

        let mut swarm = {
            // Create a Kademlia behaviour.
            let mut cfg = KademliaConfig::default();
            cfg.set_protocol_name(b"/glycos/1.0.0".to_vec());
            let behaviour = super::GlycosBehaviour::new(
                cfg,
                relay,
                self.key_pair.public(),
                self.db,
                Mdns::new(MdnsConfig::default()).await?,
            );

            SwarmBuilder::new(transport, behaviour, local_peer_id)
                .executor(Box::new(|fut| {
                    tokio::spawn(fut);
                }))
                .build()
        };

        for addr in self.listen_on {
            swarm.listen_on(addr)?;
        }

        // Accept routed requests.
        swarm.listen_on(
            Multiaddr::empty()
                .with(libp2p::multiaddr::Protocol::P2pCircuit)
                .with(libp2p::multiaddr::Protocol::P2p(local_peer_id.into())),
        )?;

        if !cfg!(feature = "no-relay") {
            // Accept to route requests
            // XXX With the above feature, the executable should not forward requests.  This will
            // be enabled on smartphone builds.
        }

        Ok(swarm)
    }
}

/// Create the required boilerplate for a running node.
/// This contains a socket, a router, and a handler for the socket.
///
/// Returns a `NodeFacade`.
/// `NodeFacade` can be used to query the `Server`s state.
///
/// The node will stop when the associated `NodeFacade` is dropped.
pub async fn new(db: PersistentGraph) -> Result<NodeFacade, anyhow::Error> {
    Ok(NodeBuilder::default().graph(db).into_facade().await?)
}

pub async fn new_in_ram() -> Result<NodeFacade, anyhow::Error> {
    Ok(NodeBuilder::default().into_facade().await?)
}

pub async fn new_in_ram_with_port(port: u16) -> Result<NodeFacade, anyhow::Error> {
    Ok(NodeBuilder::default()
        .listen_port(port)
        .into_facade()
        .await?)
}

pub async fn new_with_port(
    port: u16,
    storage: PersistentGraph,
) -> Result<NodeFacade, anyhow::Error> {
    let swarm = NodeBuilder::default()
        .listen_port(port)
        .graph(storage)
        .into_facade()
        .await?;

    Ok(swarm)
}
