//! # Glycos
//!
//! This is a library

#![recursion_limit = "4096"]

pub mod crypto;
pub mod graph;

pub mod core;

pub mod net;

// pub mod orm;

pub const BOOTNODES: [&str; 2] = [
    "12D3KooWLiSa4jNwWQrf2vvzBekpMM7VdC2XNUUS4gj9QFYMN3WN",
    "12D3KooWGkSCFj4FXZEAeEK7jWZVkXPoQtCCZfuNFBPVaKZ1D2nm",
];
