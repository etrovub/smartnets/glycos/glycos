//! Elliptic curve cryptographic primitives
//!
//! Includes `PublicKey`, `PrivateKey`,
//! signatures (both simple and ring),
//! key agreement.
use curve25519_dalek::{constants, ristretto, scalar};
use ring::{error, rand};
use serde::de::{self, Deserialize, Deserializer, Visitor};
use serde::{Serialize, Serializer};
use sha3::{Digest, Sha3_256};
use std::fmt;
use subtle::ConstantTimeEq;

// XXX: in the whole crypto directory, to_bytes/as_bytes/bytes are used interchangably. Refactor
// such that to_bytes returns [u8; 32], as_bytes &[u8; 32].

#[path = "ec_ops.rs"]
mod ops;
use self::ops::*;

#[path = "ec_ring.rs"]
mod ring_signature;
pub use self::ring_signature::*;

#[path = "ec_ephemeral_derivation.rs"]
mod ephemeral_kd;
pub use self::ephemeral_kd::*;

macro_rules! to_hex {
    ($c:expr) => {{
        let c = $c as u8;
        if c < 10 {
            c + 0x30
        } else {
            debug_assert!(c > 9u8);
            c + 0x61 - 10
        }
    }};
}

#[derive(Eq, PartialEq, Clone, Debug)]
/// A raw public key
pub struct PublicKey {
    inner: ristretto::RistrettoPoint,
}

impl Serialize for PublicKey {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        serializer.serialize_bytes(self.inner.compress().as_bytes())
    }
}

impl<'de> Deserialize<'de> for PublicKey {
    fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<PublicKey, D::Error> {
        struct KeyVisitor;

        impl<'de> Visitor<'de> for KeyVisitor {
            type Value = PublicKey;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("32 bytes")
            }

            fn visit_bytes<E: de::Error>(self, v: &[u8]) -> Result<PublicKey, E> {
                if v.len() != 32 {
                    return Err(de::Error::missing_field("key"));
                }
                PublicKey::from_bytes(v).map_err(|_| de::Error::missing_field("key"))
            }
        }

        deserializer.deserialize_bytes(KeyVisitor)
    }
}

// XXX: several places use reduce, where we should in fact mask.

impl PublicKey {
    pub fn bytes(&self) -> [u8; 32] {
        self.inner.compress().to_bytes()
    }

    /// Verifies a SchnorrSignature
    pub fn verify(&self, sig: &SchnorrSignature, data: &[u8]) -> bool {
        // XXX: we may want this in constant time.
        // Digest the data
        let mut digest = Sha3_256::default();
        digest.update(sig.r.as_bytes());
        digest.update(data);
        let digest = scalar_from_digest(digest);

        let r_verify = &sig.s * &constants::RISTRETTO_BASEPOINT_TABLE + digest * self.inner;
        sig.r.ct_eq(&r_verify.compress()).into()
    }

    pub fn from_bytes(vec: &[u8]) -> crate::crypto::Result<Self> {
        if vec.len() != 32 {
            return Err(error::Unspecified);
        }
        let mut slice = [0u8; 32];
        slice.copy_from_slice(vec);
        Ok(PublicKey {
            inner: ristretto::CompressedRistretto(slice)
                .decompress()
                .ok_or(error::Unspecified)?,
        })
    }

    pub fn identifier(&self) -> [u8; 32] {
        let digest = Sha3_256::digest(self.inner.compress().as_bytes());
        let mut res = [0u8; 32];
        res.copy_from_slice(&digest);
        res
    }

    pub fn short_hash(&self) -> String {
        let hash = self.identifier();
        let mut short = String::with_capacity(8);
        for byte in &hash[0..4] {
            let first: u8 = to_hex!(byte & 0xF);
            let second: u8 = to_hex!((byte >> 4) & 0xFu8);
            short.push(first.into());
            short.push(second.into());
        }
        short
    }
}

#[derive(Eq, PartialEq, Debug, Clone)]
/// A raw private key
pub struct PrivateKey {
    inner: scalar::Scalar,
}

#[derive(Clone)]
pub struct SchnorrSignature {
    r: ristretto::CompressedRistretto,
    s: scalar::Scalar,
}

impl SchnorrSignature {
    pub fn bytes(&self) -> ([u8; 32], [u8; 32]) {
        (self.r.to_bytes(), self.s.to_bytes())
    }

    pub fn from_bytes(bytes: ([u8; 32], [u8; 32])) -> SchnorrSignature {
        SchnorrSignature {
            r: ristretto::CompressedRistretto(bytes.0),
            s: scalar::Scalar::from_bytes_mod_order(bytes.1),
        }
    }

    pub fn recover_public_key<D: AsRef<[u8]>>(&self, data: D) -> crate::crypto::Result<PublicKey> {
        let data = data.as_ref();
        let r = self.r.decompress().ok_or(error::Unspecified)?;

        let mut digest = Sha3_256::default();
        digest.update(self.r.as_bytes());
        digest.update(data);
        let digest = scalar_from_digest(digest);

        Ok(PublicKey {
            inner: digest.invert() * (r - &self.s * &constants::RISTRETTO_BASEPOINT_TABLE),
        })
    }
}

impl PrivateKey {
    pub fn generate<R: rand::SecureRandom>(rng: &R) -> crate::crypto::Result<Self> {
        let sk = PrivateKey {
            inner: random_scalar(rng)?,
        };
        Ok(sk)
    }

    pub fn compute_public_key(&self) -> crate::crypto::Result<PublicKey> {
        Ok(PublicKey {
            inner: &self.inner * &constants::RISTRETTO_BASEPOINT_TABLE,
        })
    }

    /// Standard ECDH
    pub fn agree_with(&self, public: &PublicKey) -> crate::crypto::Result<[u8; 32]> {
        let shared_secret = self.inner * public.inner;
        Ok(shared_secret.compress().to_bytes())
    }

    /// New style Schnorr signature
    pub fn sign<R: rand::SecureRandom>(
        &self,
        rng: &R,
        data: &[u8],
    ) -> crate::crypto::Result<SchnorrSignature> {
        let k = random_scalar(rng)?;
        let r = &k * &constants::RISTRETTO_BASEPOINT_TABLE;
        let r = r.compress();

        // XXX: Here too, we want tuple hash
        let mut digest = Sha3_256::default();
        digest.update(r.as_bytes());
        digest.update(data);

        let e = scalar_from_digest(digest);
        let s = k - self.inner * e;
        Ok(SchnorrSignature { r, s })
    }

    pub fn derive_ephemeral_pair<R: rand::SecureRandom>(
        &self,
        rng: &R,
    ) -> crate::crypto::Result<(PublicKey, Recogniser, PrivateKey)> {
        // XXX: this is inefficient, should be able to get this better
        let derivation = EphemeralKeyDerivationBuilder::default(rng)?;
        let pk = derivation.derive(&self.compute_public_key()?)?;
        let recogniser = derivation.finish()?;

        let recovery = EphemeralKeyRecovery::new(&recogniser, &self)?;
        let sk = recovery.recognise(&pk)?.unwrap();

        Ok((pk, recogniser, sk))
    }

    pub fn bytes(&self) -> [u8; 32] {
        self.inner.to_bytes()
    }

    pub fn from_bytes(b: [u8; 32]) -> PrivateKey {
        PrivateKey {
            inner: scalar::Scalar::from_bytes_mod_order(b),
        }
    }
}

#[cfg(test)]
#[path = "ec_tests.rs"]
mod tests;
