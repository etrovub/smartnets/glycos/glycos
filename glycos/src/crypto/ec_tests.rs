use ring::rand;
// use test::Bencher;

use super::*;

#[test]
fn test_generate_keypair() {
    let rng = rand::SystemRandom::new();
    let key = PrivateKey::generate(&rng);
    assert!(key.is_ok());
}

#[test]
fn test_compute_public_key() {
    let rng = rand::SystemRandom::new();
    let key = PrivateKey::generate(&rng).unwrap();

    assert!(key.compute_public_key().is_ok());
}

#[test]
fn test_serialize_public_key() {
    let rng = rand::SystemRandom::new();
    let key = PrivateKey::generate(&rng).unwrap();

    let public_key = key.compute_public_key().unwrap();
    let bytes = public_key.bytes();
    let public_key2 = PublicKey::from_bytes(&bytes).unwrap();
    assert_eq!(public_key, public_key2);
}

// #[test]
// fn test_signature() {
//     let rng = rand::SystemRandom::new();
//     let key = PrivateKey::generate(&rng).unwrap();
//     let puk = key.get_public();
//
//     const MESSAGE: &'static [u8] = b"hello, world";
//     let signature = key.sign(MESSAGE);
//
//     assert!(puk.verify(Input::from(MESSAGE), Input::from(signature.as_ref())).is_ok());
// }

#[test]
fn test_derive_public_key() {
    // let rng = test::rand::FixedByteRandom { byte: 1 };
    let rng = rand::SystemRandom::new();

    // Bob's master key. `puk` is public
    let master_key = PrivateKey::generate(&rng).unwrap();
    let puk = master_key.compute_public_key().unwrap();

    // Alice wants to generate a key for Bob. R is public, r gets thrown away.
    let ekdb = EphemeralKeyDerivationBuilder::default(&rng).unwrap();
    let bob_pub_derived = ekdb.derive(&puk);
    assert!(bob_pub_derived.is_ok());
    let bob_pub_derived = bob_pub_derived.unwrap();

    let recogniser = ekdb.finish().unwrap();

    let recovery = EphemeralKeyRecovery::new(&recogniser, &master_key).unwrap();
    let recognision = recovery.recognise(&bob_pub_derived).unwrap();
    assert!(recognision.is_some());
    let bob_sk_derived = recognision.unwrap();

    // Now check that Bob's derived public key is actually usable
    let alice_sk = PrivateKey::generate(&rng).unwrap();
    let alice_pk = alice_sk.compute_public_key().unwrap();
    let agree_1 = alice_sk.agree_with(&bob_pub_derived).unwrap();
    let agree_2 = bob_sk_derived.agree_with(&alice_pk).unwrap();
    assert_eq!(agree_1, agree_2);
}

#[test]
fn test_generate_signature() {
    let rng = rand::SystemRandom::new();

    // Alice's master key
    let sk = PrivateKey::generate(&rng).unwrap();
    let pk = sk.compute_public_key().unwrap();

    let data = b"Hello world";
    let signature = sk.sign(&rng, data).unwrap();

    assert!(pk.verify(&signature, data));
}

#[test]
fn test_recover_public_key_from_signature() {
    let rng = rand::SystemRandom::new();

    // Alice's master key
    let sk = PrivateKey::generate(&rng).unwrap();
    let pk = sk.compute_public_key().unwrap();

    let data = b"Hello world";
    let signature = sk.sign(&rng, data).unwrap();

    assert_eq!(signature.recover_public_key(data), Ok(pk));
}

#[test]
fn test_verify_altered_data_signature() {
    let rng = rand::SystemRandom::new();

    // Alice's master key
    let sk = PrivateKey::generate(&rng).unwrap();
    let pk = sk.compute_public_key().unwrap();

    let data = b"Hello world!";
    let signature = sk.sign(&rng, data).unwrap();

    let data = b"Hello world";
    assert!(!pk.verify(&signature, data));
}

#[test]
fn test_ring_signature() {
    // let rng = test::rand::FixedByteRandom { byte: 1 };
    let rng = rand::SystemRandom::new();

    let alice = PrivateKey::generate(&rng).unwrap();
    let bob = PrivateKey::generate(&rng).unwrap();
    let carol = PrivateKey::generate(&rng).unwrap();

    let alice_pk = alice.compute_public_key().unwrap();
    let _bob_pk = bob.compute_public_key().unwrap();
    let carol_pk = carol.compute_public_key().unwrap();

    let data = b"Hello world";

    let mut signer = AbeRingSigner::default();
    signer.push(&alice_pk, &rng).unwrap();
    signer.push(&carol_pk, &rng).unwrap();
    let signature = signer.finish(&bob, 1, &rng, data).unwrap();

    assert!(signature.verify(data));

    let mut signer = AbeRingSigner::default();
    signer.push_many([alice_pk, carol_pk], &rng).unwrap();
    let signature = signer.finish(&bob, 1, &rng, data).unwrap();

    assert!(signature.verify(data));
}

#[test]
fn test_altered_ring_signature() {
    // let rng = test::rand::FixedByteRandom { byte: 1 };
    let rng = rand::SystemRandom::new();

    let alice = PrivateKey::generate(&rng).unwrap();
    let bob = PrivateKey::generate(&rng).unwrap();
    let carol = PrivateKey::generate(&rng).unwrap();

    let alice_pk = alice.compute_public_key().unwrap();
    let _bob_pk = bob.compute_public_key().unwrap();
    let carol_pk = carol.compute_public_key().unwrap();

    let data = b"Hello world";

    let mut signer = AbeRingSigner::default();
    signer.push(&alice_pk, &rng).unwrap();
    signer.push(&carol_pk, &rng).unwrap();
    let signature = signer.finish(&bob, 1, &rng, data).unwrap();

    let data = b"Hello world!";

    assert!(!signature.verify(data));
}

// #[bench]
// fn bench_key_agreements(b: &mut Bencher) {
//     let rng = rand::SystemRandom::new();
//
//     let alice = PrivateKey::generate(&rng).unwrap();
//     let bob = PrivateKey::generate(&rng).unwrap();
//
//     let alice_pk = alice.compute_public_key().unwrap();
//
//     b.iter(|| bob.agree_with(&alice_pk))
// }
