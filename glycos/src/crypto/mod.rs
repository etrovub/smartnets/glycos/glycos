//! Implementation of the cryptographic primitives used by Glycos.
//!
//! Most of the most interesting primitives are contained in the
//! elliptic curve `ec` module.
use ring::error;
use std::result;

pub mod ec;

pub type Result<R> = result::Result<R, error::Unspecified>;

#[cfg(test)]
mod tests;
