use glycos::graph::primitives::*;

pub struct VertexStack {
    inner: Vec<Vertex>,
}

impl VertexStack {
    pub fn at(v: Vertex) -> VertexStack {
        VertexStack { inner: vec![v] }
    }

    pub fn iter<'s>(&'s self) -> impl Iterator<Item = &'s Vertex> + 's {
        self.inner.iter().rev()
    }

    pub fn peek(&self) -> Option<&Vertex> {
        self.inner.last()
    }

    pub fn pop(&mut self) -> Option<Vertex> {
        if self.inner.len() > 1 {
            self.inner.pop()
        } else {
            None
        }
    }
}
