use clap::{App, Arg};
use futures::prelude::*;
use ring::rand;
use rustyline::error::ReadlineError;
use rustyline::Editor;
use std::str;

use libp2p::kad::record::Key;
use log::*;

use glycos::core;
use glycos::crypto::ec::*;
use glycos::graph::cryptograph::{EdgeBuilder, VertexBuilder};
use glycos::graph::persist::PersistentGraph;
use glycos::graph::primitives::*;
use glycos::net;

mod vertex_stack;
use vertex_stack::*;

struct Explorer {
    profile_key: PrivateKey,
    vertex_stack: VertexStack,
    db: PersistentGraph,
    node: net::NodeFacade,
}

impl Explorer {
    fn new(
        profile_key: PrivateKey,
        profile: Vertex,
        db: PersistentGraph,
        node: net::NodeFacade,
    ) -> Explorer {
        Explorer {
            profile_key,
            vertex_stack: VertexStack::at(profile),
            db,
            node,
        }
    }

    async fn run_command<'a>(
        &mut self,
        command: &str,
        mut parts: impl Iterator<Item = &'a str> + 'a,
    ) -> Result<(), ()> {
        let rng = rand::SystemRandom::new();

        let root_walker = core::GraphWalker::new(
            &self.profile_key,
            &self.db,
            self.vertex_stack.peek().unwrap().identifier(),
        );

        match command {
            "quit" | "exit" => return Err(()),
            "pop" => {
                if let Some(v) = self.vertex_stack.pop() {
                    println!("pop {:?}", v);
                } else {
                    println!("already at top");
                }
            }
            "cv" => match parts.next() {
                Some(_predicate) => {}
                None => (),
            },
            "show" => {
                // XXX: More information
                let current = self.vertex_stack.peek().unwrap();
                println!("{:?}", current);
            }
            "ls" => match root_walker.iter() {
                Ok(Some(mut walker)) => {
                    while let Some((edge, object)) = walker.next().await {
                        println!(
                            " -> {} {}",
                            edge.predicate,
                            (object.value.as_ref())
                                .map(|v| { str::from_utf8(v).expect("Daaaaaamn") })
                                .unwrap_or(&object.short_hash())
                        );
                    }
                }
                Ok(None) => log::info!("No edges"),
                Err(e) => {
                    log::error!("Error: {:?}", e);
                }
            },
            "append" => {
                match parts.next() {
                    Some(predicate) => {
                        let value = parts.next();
                        let builder = VertexBuilder::new(
                            &rng,
                            &self.profile_key.compute_public_key().unwrap(),
                        )
                        .unwrap();
                        let (object, _) = if let Some(value) = value {
                            builder.value(value).unwrap()
                        } else {
                            builder
                        }
                        .build(&rng, &self.profile_key)
                        .expect("Could not create vertex");
                        self.db
                            .persist_vertex(&object)
                            .expect("Could not persist vertex");

                        let current = self.vertex_stack.peek().unwrap();
                        let (current, _) = self
                            .db
                            .find_vertex(current.identifier())
                            .expect("Couldn't re-retrieve vertex from cache");
                        let current = current.expect("Vertex magically disappeared from cache");

                        let edge = EdgeBuilder::default()
                            .from(&current)
                            .to(&object)
                            .label(predicate)
                            .build(
                                &rng,
                                &current
                                    .check_acl(&self.profile_key)
                                    .expect("Cryptographic error")
                                    .expect("No access to this vertex"),
                            )
                            .expect("Cryptographic error while building the edge");
                        self.db.persist_edge(&edge).expect("Could not persist edge");
                        // Persist on the network
                        self.node
                            .store_vertex(current)
                            .await
                            .expect("Problem publishing vertex on network");
                        self.node
                            .store_edge(edge)
                            .await
                            .expect("Problem publishing edge on network");
                    }
                    None => error!("append PREDICATE [VALUE]"),
                }
            }
            "print-stack" => {
                for (i, vertex) in self.vertex_stack.iter().enumerate() {
                    println!("{}: {:?}", i, vertex);
                }
            }
            c => {
                error!("Unknown command, `{}'", c);
            }
        }
        Ok(())
    }

    async fn run(&mut self) -> Result<(), ()> {
        let mut rl = Editor::<()>::new();
        let prompt = format!(
            "Glycos explore {:?} ({} connections)% ",
            self.vertex_stack.peek().unwrap(),
            self.node.connection_count(()).await.unwrap()
        );

        match rl.readline(&prompt) {
            Ok(line) => {
                let line = line.trim();
                let mut parts = line.split(char::is_whitespace);
                match parts.next() {
                    Some("") => (),
                    Some(command) => self.run_command(command, parts).await?,
                    None => (),
                };
                rl.add_history_entry(line);
                Ok(())
            }
            Err(ReadlineError::Eof) => Err(()),
            Err(e) => {
                error!("readline error: {:?}", e);
                Err(())
            }
        }
    }
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let matches = App::new("Glycos graph explorer")
        .version("0.1.0")
        .author("Ruben De Smet <ruben.de.smet@glycos.org>")
        .about("Interactive commandline graph explorer")
        .arg(
            Arg::with_name("secret-key")
                .short("k")
                .long("secret-key")
                .value_name("KEY")
                .help("Path to the secret master key to use")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("port")
                .short("p")
                .long("port")
                .value_name("PORT")
                .help("The port to listen on")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("bootstrap")
                .short("b")
                .long("bootstrap")
                .value_name("BOOTSTRAP")
                .help("Specify bootstrap nodes")
                .multiple(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .get_matches();

    env_logger::Builder::from_env(env_logger::Env::default())
        .filter(
            None,
            match matches.occurrences_of("v") {
                0 => log::LevelFilter::Error,
                1 => log::LevelFilter::Warn,
                2 => log::LevelFilter::Info,
                3 => log::LevelFilter::Debug,
                _ => log::LevelFilter::Trace,
            },
        )
        .init();

    let listen_port = matches
        .value_of("port")
        .unwrap_or("0")
        .parse()
        .expect("Listen port should be numeric");

    log::info!("Loading or initializing default key pair for peer");
    let peer_key_pair = core::load_default_peer_key_pair(true).expect("Could not load master key");

    log::info!("Loading or initializing graph cache");
    let db = core::load_default_graph().expect("Could not load persistent graph");

    let mut node = glycos::net::NodeBuilder::default()
        .graph(db.clone())
        .listen_port(listen_port)
        .key_pair(peer_key_pair)
        .into_facade()
        .await?;

    log::info!("Node started");

    let bootstraps = matches.values_of("bootstrap");
    let mut bootstrap_override = false;
    if let Some(bootstraps) = bootstraps {
        info!("Bootstrapping");
        for bootstrap in bootstraps {
            bootstrap_override = true;
            info!("Starting bootstrap with {}", bootstrap);
            let mut ma: libp2p::Multiaddr = bootstrap
                .parse()
                .expect(&format!("Could not parse {} into a Multiaddr", bootstrap));
            if let Some(libp2p::multiaddr::Protocol::P2p(p2p)) = ma.pop() {
                let p2p = libp2p::PeerId::from_multihash(p2p).unwrap();
                log::trace!("Connecting to {}", p2p);
                node.add_address((p2p, vec![ma])).await?;
            } else {
                log::error!("No peer ID in {}; use /.../p2p/PeerId format!", bootstrap);
            }
            info!("Bootstrap with {} started", bootstrap);
        }
    }

    if !bootstrap_override {
        for n in &glycos::BOOTNODES {
            node.add_address((
                n.parse()
                    .expect(&format!("Could not parse {} into a Multiaddr", n)),
                vec!["/dnsaddr/bootstrap.glycos.org.".parse().unwrap()],
            ))
            .await?;
        }
    }

    node.bootstrap(()).await?;
    info!(
        "Bootstrapping finished! Have {} connections.",
        node.connection_count(()).await.unwrap()
    );

    log::info!("Loading or initializing private profile key");
    let profile_key = core::load_default_profile_key(true).expect("Could not load profile key");

    let rng = rand::SystemRandom::new();

    log::info!("Loading or initializing profile identity key");
    let profile = match core::read_default_profile().expect("Could not load identity public key") {
        Some(public_profile_key) => {
            // There is a public profile key on disk,
            // load the corresponding secret key from cache or network.
            let (encrypted, _) = node
                .search_vertex(Key::new(&public_profile_key.identifier()))
                .await
                .expect("connected and functioning network")
                .expect("profile to exist on the network");

            info!("Profile found on network or cache.");
            encrypted
                .decrypt(&profile_key)
                .expect("decrypt profile found on network.")
        }
        None => {
            // No public profile key on disk,
            // generate a new profile for the user.
            info!("No profile found on disk. Generating one for you!");
            let (profile, sk) =
                VertexBuilder::new(&rng, &profile_key.compute_public_key().unwrap())
                    .unwrap()
                    .build(&rng, &profile_key)
                    .expect("Could not generate new profile");
            db.persist_vertex(&profile).unwrap();
            node.store_vertex(profile.clone()).await.unwrap();
            let profile = profile
                .decrypt_direct(&sk)
                .expect("Could not decrypt newly generated profile");

            core::write_default_profile(&profile.owner)
                .expect("Could not write new profile identifier to disk");
            profile
        }
    };

    let mut explorer = Explorer::new(profile_key, profile, db, node);

    while let Ok(_) = explorer.run().await {}

    Ok(())
}
