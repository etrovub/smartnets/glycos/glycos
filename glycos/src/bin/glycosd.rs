use std::env;

use log::*;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    env_logger::init();
    let peer_key_pair = glycos::core::load_default_peer_key_pair(true).expect("Could not load key");

    let db = glycos::core::load_default_graph().expect("Could not load persistent graph");

    let port = env::args()
        .skip(1)
        .next()
        .map(|i| i.parse().unwrap())
        .unwrap_or(0);

    let mut node = glycos::net::NodeBuilder::default()
        .graph(db)
        .listen_port(port)
        .key_pair(peer_key_pair)
        .into_facade()
        .await?;

    let bootstrap_nodes = env::args().skip(2);

    let mut do_bootstrap = false;
    info!("Starting bootstraps");
    // No arguments -> default bootstrap
    if env::args().len() == 1 {
        for n in &glycos::BOOTNODES {
            do_bootstrap = true;
            node.add_address((
                n.parse()
                    .expect(&format!("Could not parse {} into a Multiaddr", n)),
                vec!["/dnsaddr/bootstrap.glycos.org.".parse().unwrap()],
            ))
            .await?;
        }
    }

    for n in bootstrap_nodes {
        info!("Starting bootstrap with {}", n);
        let mut ma: libp2p::Multiaddr = n
            .parse()
            .expect(&format!("Could not parse {} into a Multiaddr", n));
        if let Some(libp2p::multiaddr::Protocol::P2p(p2p)) = ma.pop() {
            let p2p = libp2p::PeerId::from_multihash(p2p).unwrap();
            log::trace!("Connecting to {}", p2p);
            node.add_address((p2p, vec![ma])).await?;
            do_bootstrap = true;
        } else {
            log::error!("No peer ID in {}; use /.../p2p/PeerId format!", n);
        }
        info!("Bootstrap with {} done", n);
    }

    if do_bootstrap {
        node.bootstrap(()).await?;
    }
    info!("Bootstrap finished");

    node.run_forever().await.unwrap();
    Ok(())
}
