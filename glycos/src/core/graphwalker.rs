use crate::crypto::ec::*;
use crate::graph::persist::{PersistError, PersistentGraph};
use crate::graph::primitives::*;

use futures::prelude::*;

#[derive(Debug)]
pub enum GraphWalkerError {
    PersistError,
    VertexNotFoundError,
    CryptoGraphicError,
}

impl From<PersistError> for GraphWalkerError {
    fn from(_e: PersistError) -> GraphWalkerError {
        GraphWalkerError::PersistError
    }
}

impl From<ring::error::Unspecified> for GraphWalkerError {
    fn from(_e: ring::error::Unspecified) -> GraphWalkerError {
        GraphWalkerError::CryptoGraphicError
    }
}

#[derive(Clone)]
pub struct GraphWalker<'p, K> {
    profile_key: &'p PrivateKey,
    cache: &'p PersistentGraph,
    root: K,
}

impl<'p, K: AsRef<[u8]>> GraphWalker<'p, K> {
    pub fn new(
        profile_key: &'p PrivateKey,
        cache: &'p PersistentGraph,
        root: K,
    ) -> GraphWalker<'p, K> {
        GraphWalker {
            profile_key,
            cache,
            root,
        }
    }

    pub fn iter<'s: 'p>(
        &'s self,
    ) -> Result<Option<impl Stream<Item = (Edge, Vertex)> + 's + 'p>, GraphWalkerError> {
        let (current, edges) = self.cache.find_vertex(self.root.as_ref())?;
        let current = current.ok_or(GraphWalkerError::VertexNotFoundError)?;
        let current = current.decrypt(self.profile_key)?;
        let access_key = current
            .edge_access_key
            .expect("No edge access key. Not sure what this means. FIXME");

        match edges {
            Some(edges) => Ok(Some({
                let edges = stream::iter(edges);
                edges.filter_map(move |edge| match edge {
                    Ok(edge) => match edge.decrypt(&access_key) {
                        Ok(edge) => {
                            let (object, _) = self
                                .cache
                                .find_vertex(edge.object.identifier())
                                .expect("Database error");
                            let object = object
                                .expect("Couldn't find edge object in db; FIXME")
                                .decrypt(self.profile_key)
                                .expect("Could not decrypt object");
                            futures::future::ready(Some((edge, object)))
                        }
                        Err(_) => {
                            log::warn!("decryption failure");
                            futures::future::ready(None)
                        }
                    },
                    Err(e) => {
                        log::error!("Local cache corruption error {:?}", e);
                        futures::future::ready(None)
                    }
                })
            })),
            None => Ok(None),
        }
    }
}
