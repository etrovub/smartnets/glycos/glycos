//! Plain primitives of the Glycos graph.

use std::fmt::{self, Debug, Formatter};
use std::str;

use crate::crypto::ec::*;

pub struct Vertex {
    pub owner: PublicKey,
    pub value: Option<Vec<u8>>,
    pub clock: u32,
    pub edge_access_key: Option<PrivateKey>,
    // XXX: how to define what access is granted by this key?
}

impl Vertex {
    /// Gives a short representation of this vertex.
    /// This representation is *not* collission resistant.
    pub fn short_hash(&self) -> String {
        self.owner.short_hash()
    }

    pub fn identifier(&self) -> [u8; 32] {
        self.owner.identifier()
    }
}

impl Debug for Vertex {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        let short_hash = self.short_hash();
        if let Some(value) = self.value.as_ref() {
            if let Ok(s) = str::from_utf8(value) {
                write!(f, "<#Vertex {} `{}'>", short_hash, s)
            } else {
                write!(f, "<#Vertex {} invalid-utf8>", short_hash)
            }
        } else {
            write!(f, "<#Vertex {}>", short_hash)
        }
    }
}

pub struct Edge {
    pub subject: PublicKey,
    pub predicate: String,
    pub object: PublicKey,
}

impl Debug for Edge {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        write!(
            f,
            "<#Edge {} -> {}>",
            self.predicate,
            self.object.short_hash()
        )
    }
}

#[cfg(test)]
#[path = "primitives_tests.rs"]
mod tests;
