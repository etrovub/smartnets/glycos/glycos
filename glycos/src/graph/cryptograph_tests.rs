use super::*;
use ring::rand;

#[test]
fn test_create_vertex() {
    // Scenario: Alice creates a vertex
    let rng = rand::SystemRandom::new();

    let alice = PrivateKey::generate(&rng).unwrap();

    let (wall, wall_key) = VertexBuilder::new(&rng, &alice.compute_public_key().unwrap())
        .unwrap()
        .value(Vec::from("Hello world"))
        .unwrap()
        .build(&rng, &alice)
        .unwrap();

    assert!(wall.verify_signature());

    assert_eq!(wall_key, wall.check_acl(&alice).unwrap().unwrap());
}

#[test]
fn test_create_simple_graph() {
    // Scenario: Alice creates a vertex
    // Alice (as owner) attaches another vertex to it.
    let rng = rand::SystemRandom::new();

    let alice = PrivateKey::generate(&rng).unwrap();
    let alice_pub = alice.compute_public_key().unwrap();

    let (wall, wall_signer) = VertexBuilder::new(&rng, &alice_pub)
        .unwrap()
        .build(&rng, &alice)
        .unwrap();

    let (person, _) = VertexBuilder::new(&rng, &alice_pub)
        .unwrap()
        .value(Vec::from("foaf:Person"))
        .unwrap()
        .build(&rng, &alice)
        .unwrap();

    let wall_test = wall.check_acl(&alice).unwrap().unwrap();
    assert_eq!(wall_signer, wall_test);

    let decrypted_person = person.decrypt(&alice).unwrap();
    assert_eq!(decrypted_person.owner, person.owner);
    assert_eq!(decrypted_person.clock, 0);
    assert_eq!(decrypted_person.value, Some("foaf:Person".into()));

    let edge = EdgeBuilder::default()
        .from(&wall)
        .to(&person)
        .label("a")
        .build(&rng, &wall_signer)
        .unwrap();

    assert_eq!(edge.acl.len(), 1);
    assert!(edge
        .acl
        .iter()
        .any(|&(ref pk, _)| pk.bytes() == wall.owner.bytes()));

    let edge = edge.decrypt(&wall_signer).unwrap();
    assert_eq!(edge.subject, wall.owner);
    assert_eq!(edge.predicate, "a");
    assert_eq!(edge.object, person.owner);

    assert!(wall.verify_signature());
}

#[test]
fn serialize_vertex() {
    let rng = rand::SystemRandom::new();
    let alice = PrivateKey::generate(&rng).unwrap();
    let bob = PrivateKey::generate(&rng).unwrap();
    let bob = bob.compute_public_key().unwrap();

    let (wall, _wall_signer) = VertexBuilder::new(&rng, &alice.compute_public_key().unwrap())
        .unwrap()
        .value(b"my wall")
        .unwrap()
        .permit(&bob)
        .unwrap()
        .build(&rng, &alice)
        .unwrap();

    let serialized = wall.as_bytes();
    let deserialized = Vertex::from_bytes(&serialized).unwrap();

    assert_eq!(wall.owner, deserialized.owner);
    assert_eq!(wall.acl, deserialized.acl);
    assert_eq!(wall.value, deserialized.value);
    assert_eq!(wall.clock, deserialized.clock);

    assert!(deserialized.verify_signature());
}

#[test]
fn serialize_edge() {
    let rng = rand::SystemRandom::new();
    let alice = PrivateKey::generate(&rng).unwrap();
    let alice_pub = alice.compute_public_key().unwrap();
    let (wall, wall_signer) = VertexBuilder::new(&rng, &alice_pub)
        .unwrap()
        .build(&rng, &alice)
        .unwrap();
    let (person, _) = VertexBuilder::new(&rng, &alice_pub)
        .unwrap()
        .value("foaf:Person")
        .unwrap()
        .build(&rng, &alice)
        .unwrap();

    let edge = EdgeBuilder::default()
        .from(&wall)
        .to(&person)
        .label("a")
        .build(&rng, &wall_signer)
        .unwrap();

    let ser = edge.as_bytes();
    let deser = Edge::from_bytes(&ser).unwrap();
    assert_eq!(edge.spo, deser.spo);
    assert_eq!(edge.acl, deser.acl);
    // XXX: check other fields

    assert!(deser.verify_signature());
}

#[test]
fn bob_appends_to_alice_wall() {
    let rng = rand::SystemRandom::new();
    let alice = PrivateKey::generate(&rng).unwrap();
    let alice_pub = alice.compute_public_key().unwrap();
    let bob = PrivateKey::generate(&rng).unwrap();
    let bob_public = bob.compute_public_key().unwrap();
    let mallory = PrivateKey::generate(&rng).unwrap();

    let (wall, wall_signer) = VertexBuilder::new(&rng, &alice_pub)
        .unwrap()
        .permit(&bob_public)
        .unwrap()
        .build(&rng, &alice)
        .unwrap();

    let (post, _) = VertexBuilder::new(&rng, &bob_public)
        .unwrap()
        .value(Vec::from("Happy BD!"))
        .unwrap()
        .build(&rng, &bob)
        .unwrap();

    let bob_ephemeral = wall
        .check_acl(&bob)
        .unwrap()
        .expect("Bob does not find himself in the Vertex ACL");

    let edge = EdgeBuilder::default()
        .from(&wall)
        .to(&post)
        .label("glycos::wallPost")
        .build(&rng, &bob_ephemeral)
        .unwrap();

    assert!(edge.verify_signature());

    // Test from Alice's perspective.
    {
        let wall_secret = wall.check_acl(&alice).unwrap().unwrap();
        assert_eq!(wall_secret, wall_signer);
        let wall = wall.decrypt(&alice).unwrap();
        let edge = edge.decrypt(&wall_secret).unwrap();
        assert_eq!(edge.subject, wall.owner);
        assert_eq!(edge.predicate, "glycos::wallPost");
    }

    // Test from Bob's perspective
    {
        let wall_secret = wall.check_acl(&bob).unwrap().unwrap();
        assert_ne!(wall_secret, wall_signer);
        let wall = wall.decrypt(&bob).unwrap();
        let edge = edge.decrypt(&wall_secret).unwrap();
        assert_eq!(edge.subject, wall.owner);
        assert_eq!(edge.predicate, "glycos::wallPost");
    }

    // Test from Mallory's perspective
    {
        let wall_secret = wall.check_acl(&mallory).unwrap();
        assert!(wall_secret.is_none());
    }
}

#[test]
fn alice_grants_bob_access() {
    let rng = rand::SystemRandom::new();
    let alice = PrivateKey::generate(&rng).unwrap();
    let alice_pub = alice.compute_public_key().unwrap();
    let bob = PrivateKey::generate(&rng).unwrap();
    let bob_public = bob.compute_public_key().unwrap();

    let (wall, _wall_signer) = VertexBuilder::new(&rng, &alice_pub)
        .unwrap()
        .value(b"my wall")
        .unwrap()
        .build(&rng, &alice)
        .unwrap();

    assert!(wall.check_acl(&bob).unwrap().is_none());

    let (wall_new, wall_signer) = VertexBuilder::update_vertex(&rng, &wall, &alice)
        .unwrap()
        .permit(&bob_public)
        .unwrap()
        .build(&rng, &alice)
        .unwrap();

    assert_eq!(wall.identifier(), wall_new.identifier());
    let wall = wall_new;
    assert_eq!(wall.clock, 1);

    let wall_secret = wall.check_acl(&bob).unwrap().unwrap();
    assert_ne!(wall_secret, wall_signer);
    let wall = wall.decrypt(&bob).unwrap();
    assert_eq!(&wall.value.unwrap(), b"my wall");
}

#[test]
fn alice_expands_acl() {
    let rng = rand::SystemRandom::new();
    let alice = PrivateKey::generate(&rng).unwrap();
    let alice_pub = alice.compute_public_key().unwrap();
    let bob = PrivateKey::generate(&rng).unwrap();
    let bob_public = bob.compute_public_key().unwrap();

    let (wall, _wall_signer) = VertexBuilder::new(&rng, &alice_pub)
        .unwrap()
        .value(b"my wall")
        .unwrap()
        .build(&rng, &alice)
        .unwrap();

    let (post, _post_signer) = VertexBuilder::new(&rng, &alice_pub)
        .unwrap()
        .value(b"my post")
        .unwrap()
        .build(&rng, &alice)
        .unwrap();

    let alice_ephemeral = wall
        .check_acl(&alice)
        .unwrap()
        .expect("Bob does not find himself in the Vertex ACL");

    let edge = EdgeBuilder::default()
        .from(&wall)
        .to(&post)
        .label("glycos::wallPost")
        .build(&rng, &alice_ephemeral)
        .unwrap();

    assert!(edge.verify_is_subject(&wall));

    let (wall, _wall_signer) = VertexBuilder::update_vertex(&rng, &wall, &alice)
        .unwrap()
        .permit(&bob_public)
        .unwrap()
        .build(&rng, &alice)
        .unwrap();

    assert!(!edge.verify_is_subject(&wall));

    let edge = EdgeBuilder::default()
        .from(&wall)
        .to(&post)
        .label("glycos::wallPost")
        .build(&rng, &alice_ephemeral)
        .unwrap();

    assert!(edge.verify_is_subject(&wall));
}
