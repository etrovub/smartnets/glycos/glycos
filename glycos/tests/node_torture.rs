use std::sync::Once;
use std::time::Duration;

use futures::prelude::*;
use futures::stream::FuturesUnordered;

use glycos::crypto::ec::PrivateKey;
use glycos::graph::cryptograph::{EdgeBuilder, VertexBuilder};
use glycos::net::{self, NodeFacade};
use libp2p::kad::record::Key;
use ring::rand;

use log::*;

use tokio::time::timeout;

static INIT: Once = Once::new();

/// Setup function that is only run once, even if called multiple times.
fn setup() {
    INIT.call_once(|| {
        env_logger::init();
    });
}

macro_rules! timeout_sync {
    ($e:expr) => {
        timeout_sync!($e, 3000)
    };
    ($e:expr, $timeout:expr) => {{
        let e = $e;
        let fut = tokio::spawn(async move {
            debug!("waiting for {}", stringify!($e));
            e.await
        });

        timeout(Duration::from_millis($timeout), fut)
            .await
            .unwrap()
            .expect("request timed out")
    }};
}

async fn bootstrap_nodes(count: usize) -> anyhow::Result<Vec<NodeFacade>> {
    let mut nodes = Vec::with_capacity(count);
    for _ in 0..count {
        nodes.push(net::new_in_ram().boxed().await?);
    }

    let bootstraps = FuturesUnordered::new();
    let mut last = nodes.pop().unwrap();

    let last_addr = last.local_addr(()).await.unwrap();
    let first_addr = nodes[0].local_addr(()).await.unwrap();

    // Add addresses like this:
    // - last gets to know first
    // - everyone (including first) gets to know last
    last.add_address(first_addr).await?;
    bootstraps.push(tokio::spawn(last.bootstrap(())));
    for control in nodes.iter_mut() {
        control.add_address(last_addr.clone()).await?;
        bootstraps.push(tokio::spawn(control.bootstrap(())));
    }

    let bootstraps = timeout_sync!(
        bootstraps
            .map(|r| if let Err(e) = r.unwrap() {
                log::error!("Bootstrap error {:?}", e);
            })
            .collect::<Vec<_>>(),
        20000
    );
    assert_eq!(bootstraps.len(), nodes.len() + 1);
    last.bootstrap(()).await.unwrap();
    nodes.push(last);

    // XXX Very number, much magic
    tokio::time::sleep(Duration::from_millis(1000)).await;

    Ok(nodes)
}

#[cfg_attr(not(feature = "expensive-tests"), ignore)]
#[tokio::test]
async fn bootstrapping() -> anyhow::Result<()> {
    setup();

    let count = 8;
    let mut nodes = bootstrap_nodes(count).await?;
    assert_eq!(nodes.len(), count);

    for (i, node) in nodes.iter_mut().enumerate() {
        let cnt = node.connection_count(()).await?;
        assert!(
            cnt >= std::cmp::min(count - 1, 20),
            "Node {} only knows {} node(s).",
            i,
            cnt
        );
    }

    Ok(())
}

#[cfg_attr(not(feature = "expensive-tests"), ignore)]
#[tokio::test]
async fn test_store_retrieve() -> anyhow::Result<()> {
    setup();
    let rng = rand::SystemRandom::new();

    // Tested up to 512
    let count = 16;
    let mut nodes = bootstrap_nodes(count).await?;
    let mut first_node = nodes.pop().unwrap();

    // Prepare vertices and edges.
    let alice = PrivateKey::generate(&rng).unwrap();
    let alice_pub = alice.compute_public_key().unwrap();
    let (wall, wall_signer) = VertexBuilder::new(&rng, &alice_pub)
        .unwrap()
        .build(&rng, &alice)
        .unwrap();
    let (person, _) = VertexBuilder::new(&rng, &alice_pub)
        .unwrap()
        .value("foaf:Person")
        .unwrap()
        .build(&rng, &alice)
        .unwrap();

    let edge = EdgeBuilder::default()
        .from(&wall)
        .to(&person)
        .label("a")
        .build(&rng, &wall_signer)
        .unwrap();

    let decrypted_wall = wall
        .decrypt(&alice)
        .expect("Could not decrypt self generated wall");

    // Store stuff
    timeout_sync!(first_node.store_vertex(wall)).unwrap();
    timeout_sync!(first_node.store_vertex(person.clone())).unwrap();
    timeout_sync!(first_node.store_edge(edge)).unwrap();

    // Retrieve stuff
    let (wall_retrieved, edges) =
        timeout_sync!(first_node.search_vertex(Key::new(&decrypted_wall.identifier())))
            .expect("Search vertex returned an error")
            .expect("Network returned no data");
    let _wall_retrieved = wall_retrieved
        .decrypt(&alice)
        .expect("Could not decrypt received profile");

    let edges: Vec<_> = edges
        .into_iter()
        .map(|edge| edge.decrypt(&wall_signer))
        .map(Result::unwrap)
        .collect();

    assert_eq!(edges.len(), 1);
    let edge_retrieved = &edges[0];
    assert_eq!(edge_retrieved.subject, decrypted_wall.owner);

    Ok(())
}

#[cfg_attr(not(feature = "expensive-tests"), ignore)]
#[tokio::test]
async fn test_store_update_retrieve_vertex() -> anyhow::Result<()> {
    setup();
    let rng = rand::SystemRandom::new();

    // Tested up to 512
    let count = 16;

    let mut nodes = bootstrap_nodes(count).await?;
    let mut first_node = nodes.pop().unwrap();

    // Prepare vertices and edges.
    let alice = PrivateKey::generate(&rng).unwrap();
    let alice_pub = alice.compute_public_key().unwrap();
    let (wall, _wall_signer) = VertexBuilder::new(&rng, &alice_pub)
        .unwrap()
        .build(&rng, &alice)
        .unwrap();

    debug!("Wall1: {}", z85::encode(&wall.identifier()));

    let decrypted_wall = wall
        .decrypt(&alice)
        .expect("Could not decrypt self generated wall");

    // Store stuff
    timeout_sync!(first_node.store_vertex(wall.clone())).unwrap();

    // Retrieve stuff
    let (wall_retrieved, edges) =
        timeout_sync!(first_node.search_vertex(Key::new(&decrypted_wall.identifier())))
            .expect("Search vertex returned an error")
            .expect("Network returned no data");
    let wall_retrieved = wall_retrieved
        .decrypt(&alice)
        .expect("Could not decrypt received profile");
    assert_eq!(wall.identifier(), wall_retrieved.identifier());

    assert_eq!(edges.len(), 0);
    assert_eq!(wall_retrieved.clock, 0);

    let (wall2, _wall_signer2) = VertexBuilder::update_vertex(&rng, &wall, &alice)
        .unwrap()
        .value("blaat")
        .unwrap()
        .build(&rng, &alice)
        .unwrap();

    debug!("Wall2: {}", z85::encode(&wall2.identifier()));

    let decrypted_wall2 = wall2
        .decrypt(&alice)
        .expect("Could not decrypt self generated wall");

    assert_eq!(wall.identifier(), wall2.identifier());

    // Store stuff
    timeout_sync!(first_node.store_vertex(wall2.clone())).unwrap();

    let (wall_retrieved, _edges) =
        timeout_sync!(first_node.search_vertex(Key::new(&decrypted_wall2.identifier())))
            .expect("Search vertex returned an error")
            .expect("Network returned no data");
    let wall_retrieved = wall_retrieved
        .decrypt(&alice)
        .expect("Could not decrypt received profile");

    assert_eq!(wall_retrieved.clock, 1);
    assert_eq!(wall_retrieved.clock, wall2.clock);

    Ok(())
}
