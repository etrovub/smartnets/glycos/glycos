use app_dirs::*;
use glycos::core;

const APP_INFO: AppInfo = AppInfo {
    name: "glycos",
    author: "glycos",
};

fn temp_dir() -> tempdir::TempDir {
    tempdir::TempDir::new("glycos-integration-test").expect("Could not create temporary directory")
}

// XXX: win/mac test support
fn mock_xdg_env() -> Vec<tempdir::TempDir> {
    let mocks = ["XDG_DATA_HOME", "XDG_CONFIG_HOME", "XDG_CACHE_HOME"];

    mocks
        .iter()
        .map(|varname| {
            let tmp = temp_dir();
            std::env::set_var(varname, tmp.path());
            tmp
        })
        .collect()
}

fn test_xdg_mock(paths: &[tempdir::TempDir]) {
    for path in paths.iter() {
        assert!(path.path().is_dir());
    }
}

fn test_create_and_retrieve_peer_key() {
    let config_path = app_root(AppDataType::UserConfig, &APP_INFO).unwrap();
    let key_path = config_path.join("peer.key");

    assert!(!key_path.is_file(), "{}", key_path.display());
    let key1 = core::load_default_peer_key_pair(true).unwrap();
    assert!(key_path.is_file(), "{}", key_path.display());
    let key2 = core::load_default_peer_key_pair(true).unwrap();
    assert_eq!(key1.public(), key2.public());
}

fn main() {
    if !cfg!(target_os = "linux") {
        return;
    }

    let guard = mock_xdg_env();

    test_xdg_mock(&guard);
    test_create_and_retrieve_peer_key();
}
