use criterion::{criterion_group, criterion_main};
use criterion::{Benchmark, Criterion, Throughput};

use glycos::crypto::ec;
use ring::rand::SecureRandom;

fn compute_public_key(c: &mut Criterion) {
    c.bench_function("compute-public-key", |b| {
        let rng = ring::rand::SystemRandom::new();

        // Bob's master key. `puk` is public
        let master_key = ec::PrivateKey::generate(&rng).unwrap();

        b.iter(|| master_key.compute_public_key().unwrap());
    });
}

fn derive_ephemeral(c: &mut Criterion) {
    c.bench_function("derive-ephemeral", |b| {
        let rng = ring::rand::SystemRandom::new();

        // Bob's master key. `puk` is public
        let master_key = ec::PrivateKey::generate(&rng).unwrap();
        let puk = master_key.compute_public_key().unwrap();

        // Alice wants to generate a key for Bob. R is public, r gets thrown away.
        let ekdb = ec::EphemeralKeyDerivationBuilder::default(&rng).unwrap();
        b.iter(|| ekdb.derive(&puk).unwrap());
    });
}

fn recovery(c: &mut Criterion) {
    c.bench_function("key-recovery", |b| {
        // let rng = test::rand::FixedByteRandom { byte: 1 };
        let rng = ring::rand::SystemRandom::new();

        // Bob's master key. `puk` is public
        let master_key = ec::PrivateKey::generate(&rng).unwrap();
        let puk = master_key.compute_public_key().unwrap();

        // Alice wants to generate a key for Bob. R is public, r gets thrown away.
        let ekdb = ec::EphemeralKeyDerivationBuilder::default(&rng).unwrap();
        let bob_pub_derived = ekdb.derive(&puk);
        assert!(bob_pub_derived.is_ok());
        let bob_pub_derived = bob_pub_derived.unwrap();

        let recogniser = ekdb.finish().unwrap();

        b.iter(|| {
            let recovery = ec::EphemeralKeyRecovery::new(&recogniser, &master_key).unwrap();
            let recognision = recovery.recognise(&bob_pub_derived).unwrap();
            recognision.unwrap()
        });
    });
}

fn bench_data() -> Vec<u8> {
    let rng = ring::rand::SystemRandom::new();
    let mut d = vec![0u8; 10000];
    rng.fill(&mut d).unwrap();
    d
}

fn public_key_signature_recovery(c: &mut Criterion) {
    let data = bench_data();
    let len = data.len();
    c.bench(
        "signature-key-recovery",
        Benchmark::new("signature-key-recovery", move |b| {
            let rng = ring::rand::SystemRandom::new();

            let sk = ec::PrivateKey::generate(&rng).unwrap();
            let _pk = sk.compute_public_key().unwrap();

            let signature = sk.sign(&rng, &data).unwrap();

            b.iter(|| signature.recover_public_key(&data).unwrap())
        })
        .throughput(Throughput::Bytes(len as u64)),
    );
}

criterion_group!(
    ec,
    compute_public_key,
    derive_ephemeral,
    recovery,
    public_key_signature_recovery,
);
criterion_main!(ec);
