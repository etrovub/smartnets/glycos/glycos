use criterion::{criterion_group, criterion_main};
use criterion::{Benchmark, Criterion, Throughput};

use glycos::crypto::ec;
use glycos::graph::cryptograph::*;

fn edge_encrypt(c: &mut Criterion) {
    let rng = ring::rand::SystemRandom::new();

    let alice = ec::PrivateKey::generate(&rng).unwrap();
    let alice_pub = alice.compute_public_key().unwrap();
    let bob = ec::PrivateKey::generate(&rng).unwrap();
    let bob_pub = bob.compute_public_key().unwrap();

    let (alice_wall, alice_wall_sign) = VertexBuilder::new(&rng, &alice_pub)
        .unwrap()
        .value(b"My Profile")
        .unwrap()
        .permit(&bob_pub)
        .unwrap()
        .build(&rng, &alice)
        .unwrap();

    let (bobs_post, _) = VertexBuilder::new(&rng, &bob_pub)
        .unwrap()
        .value(b"Bobs post")
        .unwrap()
        .permit(&alice_pub)
        .unwrap()
        .build(&rng, &bob)
        .unwrap();

    let edge = EdgeBuilder::default()
        .from(&alice_wall)
        .to(&bobs_post)
        .label("glycos:hasPost")
        .build(&rng, &alice_wall_sign)
        .unwrap();

    // Cases are name - vertex - decryptor
    // Cases for decrypt-edge
    let cases = vec![
        ("as-owner", alice.clone(), alice_wall.clone(), edge.clone()),
        ("as-friend", bob.clone(), alice_wall.clone(), edge.clone()),
    ];

    c.bench_function_over_inputs(
        "encrypt-edge",
        move |b, acl_size: &usize| {
            let members = (0..*acl_size)
                .map(|_| ec::PrivateKey::generate(&rng).unwrap())
                .map(|sk| sk.compute_public_key().unwrap())
                .collect::<Vec<_>>();

            let (alice_wall, alice_wall_sign) = VertexBuilder::new(&rng, &alice_pub)
                .unwrap()
                .value(b"My Profile")
                .unwrap()
                .permit_many(&members)
                .unwrap()
                .build(&rng, &alice)
                .unwrap();

            b.iter(|| {
                EdgeBuilder::default()
                    .from(&alice_wall)
                    .to(&bobs_post)
                    .label("glycos:hasPost")
                    .build(&rng, &alice_wall_sign)
                    .unwrap()
            });
        },
        (0..50).step_by(5),
    );

    for (case, key, wall, edge) in cases.into_iter() {
        c.bench(
            "decrypt-edge",
            Benchmark::new(case, move |b| {
                let key = wall.check_acl(&key).unwrap().unwrap();
                b.iter(|| edge.decrypt(&key).unwrap());
            }),
        );
    }
}

fn vertex_encrypt_ops(c: &mut Criterion) {
    let rng = ring::rand::SystemRandom::new();

    let alice = ec::PrivateKey::generate(&rng).unwrap();
    let alice_pub = alice.compute_public_key().unwrap();
    let bob = ec::PrivateKey::generate(&rng).unwrap();
    let bob_pub = bob.compute_public_key().unwrap();

    c.bench_function("encrypt-vertex", move |b| {
        b.iter(|| {
            VertexBuilder::new(&rng, &alice_pub)
                .unwrap()
                .value(b"My Profile")
                .unwrap()
                .permit(&bob_pub)
                .unwrap()
                .build(&rng, &alice)
                .unwrap()
        });
    });
}

fn vertex_decrypt_ops(c: &mut Criterion) {
    let rng = ring::rand::SystemRandom::new();

    let alice = ec::PrivateKey::generate(&rng).unwrap();
    let alice_pub = alice.compute_public_key().unwrap();
    let bob = ec::PrivateKey::generate(&rng).unwrap();
    let bob_pub = bob.compute_public_key().unwrap();

    let (alice_wall, _) = VertexBuilder::new(&rng, &alice_pub)
        .unwrap()
        .value(b"My Profile")
        .unwrap()
        .permit(&bob_pub)
        .unwrap()
        .build(&rng, &alice)
        .unwrap();

    // Cases are name - vertex - decryptor
    let cases = vec![
        ("as-owner", alice_wall.clone(), alice),
        ("as-friend", alice_wall, bob),
    ];

    for (case, vertex, decryptor) in cases.clone().into_iter() {
        c.bench(
            "decrypt-vertex",
            Benchmark::new(case, move |b| {
                b.iter(|| vertex.decrypt(&decryptor).unwrap());
            })
            .throughput(Throughput::Elements(1)),
        );
    }

    for (case, vertex, decryptor) in cases.into_iter() {
        c.bench(
            "check-acl",
            Benchmark::new(case, move |b| {
                b.iter(|| vertex.check_acl(&decryptor).unwrap().unwrap());
            })
            .throughput(Throughput::Elements(1)),
        );
    }
}

fn verify(c: &mut Criterion) {
    let rng = ring::rand::SystemRandom::new();

    let alice = ec::PrivateKey::generate(&rng).unwrap();
    let alice_pub = alice.compute_public_key().unwrap();
    let bob = ec::PrivateKey::generate(&rng).unwrap();
    let bob_pub = bob.compute_public_key().unwrap();

    let (alice_wall, alice_wall_sign) = VertexBuilder::new(&rng, &alice_pub)
        .unwrap()
        .value(b"My Profile")
        .unwrap()
        .permit(&bob_pub)
        .unwrap()
        .build(&rng, &alice)
        .unwrap();

    let edge = EdgeBuilder::default()
        .from(&alice_wall)
        .to(&alice_wall)
        .label("link")
        .build(&rng, &alice_wall_sign)
        .unwrap();

    c.bench_function("verify-vertex-signature", move |b| {
        b.iter(|| alice_wall.verify_signature());
    });

    c.bench_function("verify-edge-signature", move |b| {
        b.iter(|| edge.verify_signature());
    });
}

criterion_group!(
    cryptograph,
    edge_encrypt,
    vertex_decrypt_ops,
    vertex_encrypt_ops,
    verify,
);
criterion_main!(cryptograph);
