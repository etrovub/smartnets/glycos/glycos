use criterion::Criterion;
use criterion::{criterion_group, criterion_main};

use glycos::crypto::ec;
use ring::rand::SecureRandom;

fn sign(c: &mut Criterion) {
    c.bench_function_over_inputs(
        "schnorr-sign",
        move |b, data_len| {
            let rng = ring::rand::SystemRandom::new();

            let mut data = vec![0u8; *data_len];
            rng.fill(&mut data).unwrap();

            let alice = ec::PrivateKey::generate(&rng).unwrap();

            b.iter(move || alice.sign(&rng, &data).unwrap())
        },
        (0..8).map(|i| 16 << i),
    );
}

fn verify(c: &mut Criterion) {
    c.bench_function_over_inputs(
        "schnorr-verify",
        move |b, data_len| {
            let rng = ring::rand::SystemRandom::new();

            let mut data = vec![0u8; *data_len];
            rng.fill(&mut data).unwrap();

            let alice = ec::PrivateKey::generate(&rng).unwrap();

            let data = b"Hello, world!";

            let signature = alice.sign(&rng, data).unwrap();

            let alice = alice.compute_public_key().unwrap();

            b.iter(move || assert!(alice.verify(&signature, data)))
        },
        (0..8).map(|i| 16 << i),
    );
}

criterion_group!(schnorr, sign, verify);
criterion_main!(schnorr);
