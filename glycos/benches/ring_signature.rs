use std::time::Duration;

use criterion::Criterion;
use criterion::{criterion_group, criterion_main};
use glycos::crypto::ec;

fn sign(c: &mut Criterion) {
    c.bench_function_over_inputs(
        "ring-sign",
        move |b, ring_size: &usize| {
            let rng = ring::rand::SystemRandom::new();
            let alice = ec::PrivateKey::generate(&rng).unwrap();

            let members = (0..*ring_size)
                .map(|_| ec::PrivateKey::generate(&rng).unwrap())
                .map(|sk| sk.compute_public_key().unwrap())
                .collect::<Vec<_>>();

            let data = b"Hello, world!";

            b.iter(move || {
                let mut signer = ec::AbeRingSigner::default();
                signer.push_many(&members, &rng).unwrap();
                signer.finish(&alice, ring_size / 2, &rng, data).unwrap()
            })
        },
        (5..50).step_by(5),
    );
}

fn verify(c: &mut Criterion) {
    c.bench_function_over_inputs(
        "ring-verify",
        move |b, ring_size: &usize| {
            let rng = ring::rand::SystemRandom::new();
            let alice = ec::PrivateKey::generate(&rng).unwrap();

            let members = (0..*ring_size)
                .map(|_| ec::PrivateKey::generate(&rng).unwrap())
                .map(|sk| sk.compute_public_key().unwrap())
                .collect::<Vec<_>>();

            let data = b"Hello, world!";

            let mut signer = ec::AbeRingSigner::default();
            signer.push_many(&members, &rng).unwrap();
            let signature = signer.finish(&alice, ring_size / 2, &rng, data).unwrap();

            b.iter(move || assert!(signature.verify(data)))
        },
        (5..50).step_by(5),
    );
}

criterion_group!(name = ring;
                 config = Criterion::default().warm_up_time(Duration::from_millis(500));
                 targets = sign, verify);
criterion_main!(ring);
